# Week 8 and 9
These two weeks, we have been finalizing our goals with the client. Additionally, Julio was able to move many of the processes we initially created in R to Python. This will make it easier to do some of the 
follow on work we have discussed with the client.

We have also begun work on our presentation to the UVA research group. The plan is to show a draft to our current stakeholders next week and brief it to the UVA research team during the first week of April.
