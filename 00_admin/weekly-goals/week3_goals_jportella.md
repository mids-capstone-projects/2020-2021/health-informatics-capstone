Week 3 goals
* 1. Study and apply Recurrent Neural Networks RNN
* 2. I hope to accomplish it in two to three weeks because I have to read about it, run examples and then apply it into the current dataset
* 3. The goal is linked to the story **2.5.2**_Use RNN with the raw dataset and evaluate the client's response to the algorithm_ 
* 4. I think we can work as a team because we're all interested in neural networks and Machine Learning algorithms
* 5. Workload: High/ Difficulty: High/ Feasibility: Medium/ Critical: High
    * The workload is high because I have to consume tutorials and run different codes and study how to run an RNN
    * The difficulty is high beacuse we're working with a time sensible ML algorithm that we have little to no experience
    * The feasibility is medium because I don't know if the dataset is enough for a RNN, in a resarch paper they used 224 cases, we have 190.
    * I think that any Machine Learning algorithm with an accuracy over 0.7 is good but if we make a RNN that gives results before the CPET is over with a good accuracy (over .85) then it would be a major breakthrough in the project and a major reason for publishing the work
