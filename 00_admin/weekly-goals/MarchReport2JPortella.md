# Week 8/9 Summary
Ported the models into Python and balanced properly for minority cases
Provided the basic input for Joao so he can create the GUI that shows the results

# Further goals Summary
Prepare for the meeting with the rest of the team and stakeholders
Try to apply Convolutional Neural Networks into the existing data
