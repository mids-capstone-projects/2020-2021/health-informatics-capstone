## Timeline
As of semester end we are at t9 heading to t10. Our "Final Visualization" was delivered but notes were not deemed necessary. Final paper delivered.

We have begun exploring what our t10 onward will look like with only the priority to find new ML methods for the data and continue visualization work.

Our December 1st meeting will be critical in formulating our next steps.


**t1.** Research read and understood 08/21

**t2.** IRBs complete 08/28

**t3.** Initial data received (3 cases) 09/01

**t4.** Preliminary visualization for one case 09/18

Deliverable: Preliminary Visualization

**t5.** Rest of data (around 24 cases) to be received by 09/25

**t6.** Final outline for project 10/02

Deliverable: Final Outline

**t7.** All possible data scanned and digitized 10/09

Deliverable: Database

**t8.** Visualization/Vector EDA for all cases 10/23

Deliverable: Final EDA report

**t9.** End of semester paper and Phase 1 project output complete 11/13

Deliverable: End of Semester paper and Final Visualization output with notes

**t10** Explore further steps, prioritizing data/ML methods rather than domain knowledge guidance. Formulate new storyboard for 2021.


Storyboard
==========

Epic 1
------

Exercise physiology is a challenging area because of the complexity caused by the interaction between three interacting systems:
the cardiovascular system, the respiratory system and the skeletal-muscle system. To measure the response of these systems under precise conditions of metabolic stress, 
doctors use the Cardiopulmonary Exercise Testing (CPET). This is a method that evaluates interactions and responses of the 3 systems using different sensors that get different variables from energy to gas volume.
Thanks to CPET, the doctor can diagnose diseases, assert the effects of therapy, guiding the development of personalized laboratory prescriptions with more accuracy than using restrictive
measurement tools such as the electrocardiogram (ECG). Despite its benefits, CPET is hard to interpret and read correctly. Thus, our 
stakeholders would like us to use the data they collected to work on a better way to visualize data to effectively interpret CPET results.

**Our Plan**: To create the visualization improvement and any machine learning support for it, we first need to study and understand the whole CPET process. We will study from the books
and research papers provided by the stakeholders (Theme 1.1). Also, given that medical data requires IRB approval, we need to complete the IRB training in order to handle it (Theme 1.2).
We will also receive mentorship and feedback from our stakeholders who know CPET and its interpretation better than us. With the proper authorization, we will begin the data collecting process.
The dataset contains non digital records (handwritten records, printed sheets, etc), thus we will have to use a computer vision algorithm in order to digitalize the data. (Theme 1.3). 
To visualize trends, we will try to group each data in 3 vectors, each one related to one system involved in CPET. Our stakeholders will advise on which critical variables and calculations are best to visualize for each system.
After organizing and digitizing all data, we plan to set different visualization strategies and show them to a client-selected group of doctors and see if they perceive an improvement. (Theme 1.4)


### Theme 1.1 By 08/21 (t1)

CPET is a complex analysis that involves three systems. All of them are interconnected and have their own particularities. Also, during the last years, there are research projects dedicated to improve the use of CPET.
Thus, the first step is to study CPET and selected research related to this technique.

#### Story 1.1.1

ALL:
Read and study the book "Principles of Exercise Testing and Interpretation" by Karlman Wasserman et al.

#### Story 1.1.2

ALL:
Read the papers provided by Professor Hammond and West. The ones to read are:
 * Cardiopulmonary exercise testing (CPET) in the United Kingdom—a national survey of the structure, conduct, interpretation and funding
 * An Alternative Visualization to the Wasserman 9-Panel Plot for Cardiopulmonary Exercise Testing
 * Preoperative cardiopulmonary exercise testing in England – a national survey
 * Visual Analysis of FHIR Data with Open Source Web Dashboards

### Theme 1.2 By 08/28 (t2)

Working with medical data is involves many ethical issues and considerations. To access the medical data and have the approval of the IRB, we need to complete the CITI training. Then, we need to coordinate with the mentors and submit the
CITI certifications and have access to the medical data. 

#### Story 1.2.1

Complete the Duke's CITI training (Derek and Joao, Julio did it for a summer fellowship). 

#### Story 1.2.2 By 09/01 (t3)

Coordinate with professors West and Hammond in order to have access to initial data.

### Theme 1.3 Complete on an ongoing basis, Fully complete by 10/09 (t7)

We need to gather and sort the CPET data into an database. This could be a simple column based database or a relational database, it will depend on what we find in the way.
Additionally, we expect to find not only unsorted data, but also hand written or printed data that need to be digitialized. Thus we need to transform it and store it properly.
Given that this is medical data, we need to make sure that the data is safely stored and anonymized.

#### Story 1.3.1

Find an proper database to use, wether as a column based database, a collection of csv files or a relational database. 
Given the sensibility of the data, we plan to store it one of the Duke's servers. 

#### Story 1.3.2

The data is not in digital format, find computer vision techniques to digitalize the data. At the moment we have not seen any written or print data, but we have to do a research about existing 
ways to digitalize the data. *Update 9/9:* It seems data is overwhelmingly printed on PDFs so digitization will be most of the work.

#### Story 1.3.3

With the digital dataset, we have to generate the columns and sort it and clean it properly. Julio Portella worked with the [OHDSI Common Data Model](https://www.ohdsi.org/data-standardization/the-common-data-model/), thus we can try to pass the data into
the OMOP CDM model.

#### Story 1.3.4

Make sure that the data is anonymized correctly so no one can track the patient's id based on the existing information. If the data contains indentifiers, we will remove them and the dates will be altered adding a random amount of years into the pass or the future.

#### Story 1.3.5 By 10/23 (t8)

Perform an Exploratory Data Analysis (EDA) on the dataset. This will be done with the tools that we as MIDSters know. We will go through the data iterating through several different visualization types adapted for time data, including line plots and other more advanced methods as needs arise. A good results would be several different visualization we can propose and go over with the client.

### Theme 1.4 By 11/23 (t9)

Our client is seeking ways to visualize this data in a way to support real-time analysis for doctors administering the test. We will have to create a visualization for the data that is easier to interpret and which system is creating the most problems for the patient. Therefore, for this project we will create a static visualization that facilitates diagnoses for patients. It is important to know that the clients will want to use this work to apply for a research grant to use this sort of technique on a real-time basis with medical equipment and diagnoses for doctors in the future. Any ML work will be done to support doctors and make the visual experience easier and more effective.

#### Story 1.4.1

Use visualization techniques learned in MIDS courses as well as domain knowledge from client to create a useful visualization of the data.

#### Story 1.4.2

Validate if the visualization is more effective in terms of simplicity than reading charts or Wasserman plots (especially in real-time).

Epic 2
------

After, working for the last semester with Cardiopulmonary Exercise Testing (CPET) experts. We gained insights about the CPET, its purpose, pros and cons. Last semester, the team dealed with different challenges, such as understanding the stakeholder,
processing and gathering the data, testing different visualization techniques, etc. Fortunatelly, the team got good results and provided new visualization strategies that resulted in an interactive dashboard. This product was satisfactory to the client. After the winter
break, the team found new insights and ideas to add machine learning algorithm into the current work in order to make CPET interpretation better.

**Our Pan**: It is divided in three parts, to improve and expand the dataset, adding machine learning(ML) algorithms into the visualization and improving the visualization algorithms. For the first part, we will add book cases and cases from Duke (Theme 2.1). Also, we will prepare new procedures so any new group can have a better start than we did (Theme 2.2). Finally, an optional sub goal would be for us to migrate the current databse to a new format(Theme 2.3). Once the team has enough data to perform machine algorithm, we will proceed to label the data and decide where to apply ML based on the available labels (Theme 2.4). Then, we will apply different ML algorithms in order to predict the labels and find which one is useful for the stakeholders(Theme 2.5) For the visualization part, we will explore new visualization alterantives that the clients may like or find interesting(Theme 2.6). Also, we will improve the dashboard based on the latest feedback given by the client(Theme 2.7). Finally, if we have time and if there is a restriction with Tableau, we may consider migrating the dashboard into python(Theme 2.8).  

### Theme 2.1

Last semester, our main problem was the scarcity of data for running ML algorithms. Thus, we need to add more data into by two means. One way is to add more data provided by the client and another one is to add more cases from the Wasserman book. Also, we have to simplify the excel format so it can be written without too many redundancies.

#### Story 2.1.1

Update the database format, so the redundancy will be lower and better for its maintenance.

#### Story 2.1.2

Update the data provided by the clients. This is variable and it will depend on the client side

#### Store 2.1.3

Update the book cases into the database. With that our database will grow from 70 cases to 170 cases

### Theme 2.2

During the first meeting after the break, one of the things to consider is that the project is going to be longer than our current involvement. Thus, it may be useful to create manuals and procedures so the next team that works in this project may have a better start and resources than we did.

### Story 2.2.1

Create manuals and give code to help a new group to use, correct and process the existing database.

### Theme 2.3 

We're currently working with an Excel file as a database because it is very easy to update and read into the jupyter notebooks for further processing. However, for further works, we may need to translate the database into another type of database for security reasons, specially if the file is corrupted, we also need to consider the security of the database given the sensibility of the data. This goal is optional and it will depend on our time availability.

#### Story 2.3.1

Discuss with the client the databases for the migration

#### Story 2.3.2

Run an Extract Transform and Load (ETL) process to transform the existing excel database into the new database

### Theme 2.4

One critical element for running ML algorithms is to have labeled datasets without that, the predicting capabilities of the algorithm is reduced. Thus, a critical part of the project is to have labeled data into three categories. They are pulmonary, muscle-skeletal and cardiac limitation. At, the moment, after the first second meeting with the client, we will focus on the cardiac limitation because it is the only label that is well defined on the original database.

#### Story 2.4.1

Label correctly the new database with the help of the specialists

### Them 2.5

After the data is expanded and labeled, the team will run different machine learning algorithms and evaluate the client's answer to the proposed solutions. The two main approaches are the use of traditional ML algorithms such as Logistic regression, decission tree, etc. These algorithms will be used in the processed dataset. The other is to use deep learning techniques. Given that the dataset is a time series, we will use Recurrent Neural Network for the task, it was used in another research with success.

#### Story 2.5.1

Use traditional ML algorithms in processed data and evaluate the feedback from the experts

#### Story 2.5.2

Use RNN with the raw dataset and evaluate the client's response to the algorithm

### Theme 2.6

We did a dashboard with graphs that our stakeholders like it and we received feedback from their colleagues. Thus, we need to improve the dashboard and explore other visualization alternatives to see if there are other plots that can convey more information that the ones that we explored.

#### Story 2.6.1

We are going to plot the different variables and show them to the client to see if it can be used

### Theme 2.7

After receiving feedback from the stakeholders and their colleagues, we found that we need to correct some features of the current dashboard and improve some things that are working already

#### Story 2.7.1

We are going to make corrections and improvements based on the client's feedback

### Theme 2.8

At the moment we wrote this theme, the clients were happy with the Tableau dashboard, in case they need to use another platform given the limitations of Tableau and we have enough time, we may need to migrate the dashboard from Tableau into another free platform such as python.

#### Story 2.8.1

As an optional goal, we will find a Tableau alternative and discuss it with the stakeholders

#### Story 2.8.2

We will migrate the dashboard design into the new platform

